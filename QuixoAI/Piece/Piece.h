#pragma once
#include<iostream>

#ifdef PIECE_EXPORTS
#define PIECE_API __declspec(dllexport)
#else
#define PIECE_API __declspec(dllimport)
#endif // PIECE_EXPORTS

class PIECE_API Piece
{
public:
	enum class TypePiece : uint8_t
	{
		None,
		X,
		O
	};

public:
	Piece();
	Piece(TypePiece typePiece);

	Piece(const Piece &other);
	Piece(Piece && other);

	Piece & operator = (const Piece & other);
	Piece & operator = (Piece && other);

	~Piece();

	TypePiece GetTypePiece()const;
	void SetTypePiece(const TypePiece& type);

private:
	TypePiece m_typePiece : 2;
};

