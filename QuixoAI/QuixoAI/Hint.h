#pragma once
#include "..\Board\Board.h"

class Hint
{
public:
	static void HeuristicFunction(const Board& board, int& scoreX, int& scoreO);
	static void BonusPoints(const int& countX, const int& countO, int& scoreX, int& scoreY);
	static Board::Position BestPosition(const Board& board, const Piece & piece, const Board::Position & position);
	static void MoveUp(const Board& board, const Piece & piece, const Board::Position & position, Board::Position& bestPosition, int& maxScore);
	static void MoveDown(const Board& board, const Piece & piece, const Board::Position & position, Board::Position& bestPosition, int& maxScore);
	static void MoveLeft(const Board& board, const Piece & piece, const Board::Position & position, Board::Position& bestPosition, int& maxScore);
	static void MoveRight(const Board& board, const Piece & piece, const Board::Position & position, Board::Position& bestPosition, int& maxScore);

};

