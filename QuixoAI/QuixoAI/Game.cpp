#include "Game.h"
#include"../Board/Board.h"
#include"../Piece/Piece.h"
#include"Player.h"
#include"ViewGame.h"
#include"Hint.h"

#include"../Logging/Logging.h"
#include<fstream>
#include <string>

void Game::Run()
{
	Board board;

	std::ofstream of("syslog.log", std::ofstream::trunc);
	Logger logger(of);
	logger.log("Started Game...", Logger::Level::Info);

	ViewGame::ShowRules();

	std::string playerName;
	std::cout << "Enter player's X name: ";
	std::cin >> playerName;
	Player player1('X', playerName);

	std::cout << "Enter player's O name: ";
	std::cin >> playerName;
	Player player2('O', playerName);

	std::reference_wrapper<Player> playerX = player1;
	std::reference_wrapper<Player> playerO = player2;

	while (true)
	{
		system("cls");

		Piece pickedPiece;
		Board::Position position;

		ViewGame::ShowPlayingBoard(board);

		ViewGame::ShowPickingPlayer(playerX);
		while (true)
		{
			try
			{
				std::tie(pickedPiece, position) = std::move(playerX.get().PickPiece(std::cin, board));
				logger.log(playerX.get().GetName() + " is picking a piece : " + std::to_string(position.first) + " " + std::to_string(position.second), Logger::Level::Info);
				break;
			}
			catch (const char* errorMessage)
			{
				logger.log(errorMessage, Logger::Level::Warning);
				std::cout << errorMessage << std::endl;
			}
		}

		std::cout << "Do you want a hint? (Y/N) ";
		char option;
		std::cin >> option;
		while (option != 'N' && option != 'n')
		{
			if (option == 'Y' || option =='y')
			{
				Board::Position bestPosition;
				bestPosition = Hint::BestPosition(board, pickedPiece, position);
				std::cout << "You should place it on position: " << bestPosition.first << " " << bestPosition.second << std::endl;
				logger.log("Player requested hint", Logger::Level::Info);
				break;
			}
			std::cout << "Not an option. Chose Y or N. ";
			std::cin >> option;
		}

		ViewGame::ShowPlacingPlayer(playerX);

		while (true)
		{
			try
			{
				playerX.get().PlacePiece(std::cin, std::move(pickedPiece), board, position);
				logger.log(playerX.get().GetName() + " is placing the piece on the board.", Logger::Level::Info);
				break;
			}
			catch (const char* errorMessage)
			{
				logger.log(errorMessage, Logger::Level::Warning);
				std::cout << errorMessage << std::endl;
			}
		}

		switch (StateGame::Check(board))
		{
		case StateGame::State::Draw:
			ViewGame::ShowWinner(playerO);
			logger.log("Ended Game...", Logger::Level::Info);
			return;

		case StateGame::State::WonX:
			if (playerX.get().GetType() == 'X')
				ViewGame::ShowWinner(playerX);
			else
				ViewGame::ShowWinner(playerO);
			logger.log("Ended Game ...", Logger::Level::Info);
			return;

		case StateGame::State::WonO:
			if (playerX.get().GetType() == 'O')
				ViewGame::ShowWinner(playerX);
			else
				ViewGame::ShowWinner(playerO);
			logger.log("Ended Game...", Logger::Level::Info);
			return;
		}
		std::swap(playerX, playerO);
	}
}
