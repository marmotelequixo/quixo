#pragma once
#include "../Board/Board.h"

class StateGame
{
public:
	enum State {
		Playing,
		WonX,
		WonO,
		Draw
	};

public:
	static State Check(const Board& boardToCheck);
	static State CheckLine(const Board& boardToCheck, const uint8_t& line);
	static State CheckColumn(const Board& boardToCheck, const uint8_t& column);
	static State CheckDiagonalP(const Board& boardToCheck);
	static State CheckDiagonalS(const Board& boardToCheck);
};

