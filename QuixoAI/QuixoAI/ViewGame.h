#pragma once
#include"Game.h"
#include"Player.h"


class ViewGame
{
public:
	static void ShowRules();
	static void ShowPlayingBoard(const Board& board);
	static void ShowPickingPlayer(const std::reference_wrapper<Player>& player);
	static void ShowPlacingPlayer(const std::reference_wrapper<Player>& player);
	static void ShowWinner(const std::reference_wrapper<Player>& player);
};

