#include "Hint.h"

void Hint::HeuristicFunction(const Board & board, int & scoreX, int & scoreO)
{
	int countX, countO;
	scoreX = 0;
	scoreO = 0;
	Board::Position currentPosition;
	auto&[line, column] = currentPosition;
	for (line = 0; line < board.GetNumberOfRows(); line++)
	{
		for (column = 0; column < board.GetNumberOfColumns(); column++)
		{
			if (board[currentPosition].GetTypePiece() == Piece::TypePiece::X)
				scoreX += 20;
			else
				if (board[currentPosition].GetTypePiece() == Piece::TypePiece::O)
					scoreO += 20;
		}
	}

	for (column = 0; column < board.GetNumberOfColumns(); column++)
	{
		countX = 0;
		countO = 0;
		for (line = 0; line < board.GetNumberOfRows(); line++)
			if (board[currentPosition].GetTypePiece() == Piece::TypePiece::X)
				countX++;
			else
				if (board[currentPosition].GetTypePiece() == Piece::TypePiece::O)
					countO++;

		BonusPoints(countX, countO, scoreX, scoreO);
	}

	for (line = 0; line < board.GetNumberOfRows(); line++)
	{
		countX = 0;
		countO = 0;
		for (column = 0; column < board.GetNumberOfColumns(); column++)
			if (board[currentPosition].GetTypePiece() == Piece::TypePiece::X)
				countX++;
			else
				if (board[currentPosition].GetTypePiece() == Piece::TypePiece::O)
					countO++;

		BonusPoints(countX, countO, scoreX, scoreO);
	}


	//main diagonal
	countX = 0;
	countO = 0;
	for (line = 0; line < board.GetNumberOfRows(); line++)
	{
		column = line;
		if (board[currentPosition].GetTypePiece() == Piece::TypePiece::X)
			countX++;
		else
			if (board[currentPosition].GetTypePiece() == Piece::TypePiece::O)
				countO++;
	}

	BonusPoints(countX, countO, scoreX, scoreO);

	//second diagonal
	countX = 0;
	countO = 0;
	for (line = 0; line < board.GetNumberOfRows(); line++)
	{
		column = board.GetNumberOfRows() - line - 1;
		if (board[currentPosition].GetTypePiece() == Piece::TypePiece::X)
			countX++;
		else
			if (board[currentPosition].GetTypePiece() == Piece::TypePiece::O)
				countO++;
	}

	BonusPoints(countX, countO, scoreX, scoreO);
}

void Hint::BonusPoints(const int& countX, const int& countO, int & scoreX, int & scoreO)
{
	if (countX == 3)
		scoreX += 5;
	else
		if (countX == 4)
			scoreX += 15;
		else
			if (countX == 5)
				scoreX += 25;

	if (countO == 3)
		scoreO += 5;
	else
		if (countO == 4)
			scoreO += 15;
		else
			if (countO == 5)
				scoreO += 25;
}

Board::Position Hint::BestPosition(const Board& board, const Piece & piece, const Board::Position & position)
{
	const auto&[initialLine, initialColumn] = position;
	Board tempBoard=board;
	int scoreX, scoreO;
	Board::Position bestPosition;
	int maxScore=INT_MIN;
	int auxScore;

	if (initialLine == 0 && initialColumn == 0)
	{
		MoveUp(board, piece, position, bestPosition, maxScore);
		MoveLeft(board, piece, position, bestPosition, maxScore);
	}
	else
		if (initialLine == 0 && initialColumn == board.GetNumberOfColumns()-1)
		{
			MoveUp(board, piece, position, bestPosition, maxScore);
			MoveRight(board, piece, position, bestPosition, maxScore);
		}
		else
			if (initialLine == board.GetNumberOfRows()-1 && initialColumn == 0)
			{
				MoveDown(board, piece, position, bestPosition, maxScore);
				MoveLeft(board, piece, position, bestPosition, maxScore);
			}
			else
				if (initialLine == board.GetNumberOfRows() - 1 && initialColumn == board.GetNumberOfColumns() - 1)
				{
					MoveDown(board, piece, position, bestPosition, maxScore);
					MoveRight(board, piece, position, bestPosition, maxScore);
				}
				else
					if (initialLine == 0)
					{
						MoveUp(board, piece, position, bestPosition, maxScore);
						MoveRight(board, piece, position, bestPosition, maxScore);
						MoveLeft(board, piece, position, bestPosition, maxScore);
					}
					else
						if (initialLine == board.GetNumberOfRows()-1)
						{
							MoveDown(board, piece, position, bestPosition, maxScore);
							MoveRight(board, piece, position, bestPosition, maxScore);
							MoveLeft(board, piece, position, bestPosition, maxScore);
						}
						else
							if (initialColumn == 0)
							{
								MoveDown(board, piece, position, bestPosition, maxScore);
								MoveUp(board, piece, position, bestPosition, maxScore);
								MoveLeft(board, piece, position, bestPosition, maxScore);
							}
							else
								if (initialColumn == board.GetNumberOfColumns() - 1)
								{
									MoveDown(board, piece, position, bestPosition, maxScore);
									MoveRight(board, piece, position, bestPosition, maxScore);
									MoveUp(board, piece, position, bestPosition, maxScore);
								}
	return bestPosition;
}

void Hint::MoveUp(const Board & board, const Piece & piece, const Board::Position & position, Board::Position& bestPosition, int& maxScore)
{
	Board tempBoard = board;
	int scoreX, scoreO, auxScore;
	const auto&[initialLine, initialColumn] = position;
	tempBoard.PushUp(position, piece);
	HeuristicFunction(tempBoard, scoreX, scoreO);
	if (piece.GetTypePiece() == Piece::TypePiece::X)
		auxScore = scoreX - scoreO;
	else
		auxScore = scoreO - scoreX;
	if (maxScore < auxScore)
	{
		maxScore = auxScore;
		auto&[line, column] = bestPosition;
		line = board.GetNumberOfRows() - 1;
		column = initialColumn;
	}
}

void Hint::MoveDown(const Board & board, const Piece & piece, const Board::Position & position, Board::Position & bestPosition, int & maxScore)
{
	Board tempBoard = board;
	int scoreX, scoreO, auxScore;
	const auto&[initialLine, initialColumn] = position;
	tempBoard.PushDown(position, piece);
	HeuristicFunction(tempBoard, scoreX, scoreO);
	if (piece.GetTypePiece() == Piece::TypePiece::X)
		auxScore = scoreX - scoreO;
	else
		auxScore = scoreO - scoreX;
	if (maxScore < auxScore)
	{
		maxScore = auxScore;
		auto&[line, column] = bestPosition;
		line = 0;
		column = initialColumn;
	}
}

void Hint::MoveLeft(const Board & board, const Piece & piece, const Board::Position & position, Board::Position & bestPosition, int & maxScore)
{
	Board tempBoard = board;
	int scoreX, scoreO, auxScore;
	const auto&[initialLine, initialColumn] = position;
	tempBoard.PushLeft(position, piece);
	HeuristicFunction(tempBoard, scoreX, scoreO);
	if (piece.GetTypePiece() == Piece::TypePiece::X)
		auxScore = scoreX - scoreO;
	else
		auxScore = scoreO - scoreX;
	if (maxScore < auxScore)
	{
		maxScore = auxScore;
		auto&[line, column] = bestPosition;
		column = board.GetNumberOfColumns() - 1;
		line = initialLine;
	}
}

void Hint::MoveRight(const Board & board, const Piece & piece, const Board::Position & position, Board::Position & bestPosition, int & maxScore)
{
	Board tempBoard = board;
	int scoreX, scoreO, auxScore;
	const auto&[initialLine, initialColumn] = position;
	tempBoard.PushRight(position, piece);
	HeuristicFunction(tempBoard, scoreX, scoreO);
	if (piece.GetTypePiece() == Piece::TypePiece::X)
		auxScore = scoreX - scoreO;
	else
		auxScore = scoreO - scoreX;
	if (maxScore < auxScore)
	{
		maxScore = auxScore;
		auto&[line, column] = bestPosition;
		column = 0;
		line = initialLine;
	}
}
