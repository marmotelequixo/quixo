#include "ViewPlayer.h"

#include <string>

void ViewPlayer::ShowPlayer(const Player & player)
{
	std::cout << "Player: " << player.GetType() << " is " << player.GetName() << "\n";
}
