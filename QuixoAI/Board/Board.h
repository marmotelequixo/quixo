#pragma once

#include "../Piece/Piece.h"

#include <array>

#ifdef BOARD_EXPORTS
#define BOARD_API __declspec(dllexport)
#else
#define BOARD_API __declspec(dllimport)
#endif // PIECE_EXPORTS

class BOARD_API Board
{
public:
	using Position = std::pair<uint16_t, uint16_t>;

public:
	Board() = default;
	~Board();
	const Piece& operator [] (const Position& position) const;
	Piece& operator [] (const Position& position);
	Board& operator =(const Board& board);

	const size_t& GetNumberOfRows() const;
	const size_t& GetNumberOfColumns() const;

	void PushDown(const Position& initial, const Piece& piece);
	void PushUp(const Position& initial, const Piece& piece);
	void PushLeft(const Position& initial, const Piece& piece);
	void PushRight(const Position& initial, const Piece& piece);

private:
	static const size_t m_kNumberOfRows = 5;
	static const size_t m_kNumberOfColumns = 5;
	static const size_t m_kNumberOfTiles = m_kNumberOfColumns * m_kNumberOfColumns;

private:
	std::array <Piece, m_kNumberOfTiles> m_tiles;
};

