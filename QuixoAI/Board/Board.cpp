#include "Board.h"

Board::~Board()
{
}

const Piece& Board::operator[](const Position & position) const
{
	const auto&[line, column] = position;
	if (line >= m_kNumberOfRows || column >= m_kNumberOfColumns)
		throw std::out_of_range("Board index out of bound.");
	return m_tiles[m_kNumberOfColumns*line + column];
}

Piece& Board::operator[](const Position & position)
{
	const auto&[line, column] = position;
	if (line >= m_kNumberOfRows || column >= m_kNumberOfColumns)
		throw std::out_of_range("Board index out of bound.");
	return m_tiles[(m_kNumberOfColumns)*line + column];
}

Board & Board::operator=(const Board & board)
{
	for (int index = 0; index < m_kNumberOfTiles; index++)
		m_tiles[index] = board.m_tiles[index];
	return (*this);
}

const size_t& Board::GetNumberOfRows() const
{
	return m_kNumberOfRows;
}

const size_t& Board::GetNumberOfColumns() const
{
	return m_kNumberOfColumns;
}

void Board::PushDown(const Position & initial, const Piece & piece)
{
	const auto&[line, column] = initial;
	for (int index = line; index > 0; index--)
		m_tiles[index * m_kNumberOfColumns + column] = std::move(m_tiles[(index - 1)*m_kNumberOfColumns + column]);
	m_tiles[column] = piece;
}

void Board::PushUp(const Position & initial, const Piece & piece)
{
	const auto&[line, column] = initial;
	for (int index = line; index < m_kNumberOfRows - 1; index++)
		m_tiles[index * m_kNumberOfColumns + column] = std::move(m_tiles[(index + 1)*m_kNumberOfColumns + column]);
	m_tiles[m_kNumberOfColumns*(m_kNumberOfColumns - 1) + column] = piece;
}

void Board::PushLeft(const Position & initial, const Piece & piece)
{
	const auto&[line, column] = initial;
	for (int index = column; index < m_kNumberOfColumns - 1; index++)
		m_tiles[line*m_kNumberOfColumns + index] = std::move(m_tiles[line*m_kNumberOfColumns + index + 1]);
	m_tiles[line * m_kNumberOfColumns + m_kNumberOfColumns - 1] = piece;
}

void Board::PushRight(const Position & initial, const Piece & piece)
{
	const auto&[line, column] = initial;
	for (int index = column; index > 0; index--)
		m_tiles[line*m_kNumberOfColumns + index] = std::move(m_tiles[line*m_kNumberOfColumns + index - 1]);
	m_tiles[line * m_kNumberOfColumns] = piece;
}

