#include "StateGame.h"

StateGame::State StateGame::Check(const Board & boardToCheck)
{
	StateGame::State currentState = StateGame::State::Playing;
	for (int index = 0; index < boardToCheck.GetNumberOfRows(); index++)
		if (CheckLine(boardToCheck, index) != StateGame::State::Playing)
		{
			if (CheckLine(boardToCheck, index) == StateGame::State::WonX)
				if (currentState == StateGame::State::WonO)
					return StateGame::State::Draw;
				else
					currentState = StateGame::State::WonX;
			else
				if (currentState == StateGame::State::WonX)
					return StateGame::State::Draw;
				else
					currentState = StateGame::State::WonO;
		}

	for (int index = 0; index < boardToCheck.GetNumberOfColumns(); index++)
		if (CheckColumn(boardToCheck, index) != StateGame::State::Playing)
		{
			if (CheckColumn(boardToCheck, index) == StateGame::State::WonX)
				if (currentState == StateGame::State::WonO)
					return StateGame::State::Draw;
				else
					currentState = StateGame::State::WonX;
			else
				if (currentState == StateGame::State::WonX)
					return StateGame::State::Draw;
				else
					currentState = StateGame::State::WonO;
		}

	if (CheckDiagonalP(boardToCheck) != StateGame::State::Playing)
	{
		if (CheckDiagonalP(boardToCheck) == StateGame::State::WonX)
			if (currentState == StateGame::State::WonO)
				return StateGame::State::Draw;
			else
				currentState = StateGame::State::WonX;
		else
			if (currentState == StateGame::State::WonX)
				return StateGame::State::Draw;
			else
				currentState = StateGame::State::WonO;
	}

	if (CheckDiagonalS(boardToCheck) != StateGame::State::Playing)
	{
		if (CheckDiagonalS(boardToCheck) == StateGame::State::WonX)
			if (currentState == StateGame::State::WonO)
				return StateGame::State::Draw;
			else
				currentState = StateGame::State::WonX;
		else
			if (currentState == StateGame::State::WonX)
				return StateGame::State::Draw;
			else
				currentState = StateGame::State::WonO;
	}
	return currentState;
}

StateGame::State StateGame::CheckLine(const Board & boardToCheck, const uint8_t & line)
{
	Board::Position position;
	auto&[row, column] = position;
	row = line;
	column = 0;

	if (boardToCheck[position].GetTypePiece() == Piece::TypePiece::None)
		return StateGame::State::Playing;
	Piece::TypePiece pieceType = boardToCheck[position].GetTypePiece();

	for (int index = 1; index < boardToCheck.GetNumberOfColumns(); index++)
	{
		column = index;
		if (boardToCheck[position].GetTypePiece() != pieceType)
			return StateGame::State::Playing;
	}

	if (pieceType == Piece::TypePiece::X)
		return StateGame::State::WonX;

	if (pieceType == Piece::TypePiece::O)
		return StateGame::State::WonO;

}

StateGame::State StateGame::CheckColumn(const Board & boardToCheck, const uint8_t & column)
{
	Board::Position position;
	auto&[row, col] = position;
	row = 0;
	col = column;

	if (boardToCheck[position].GetTypePiece() == Piece::TypePiece::None)
		return StateGame::State::Playing;
	Piece::TypePiece pieceType = boardToCheck[position].GetTypePiece();

	for (int index = 1; index < boardToCheck.GetNumberOfRows(); index++)
	{
		row = index;
		if (boardToCheck[position].GetTypePiece() != pieceType)
			return StateGame::State::Playing;
	}

	if (pieceType == Piece::TypePiece::X)
		return StateGame::State::WonX;

	if (pieceType == Piece::TypePiece::O)
		return StateGame::State::WonO;
}

StateGame::State StateGame::CheckDiagonalP(const Board & boardToCheck)
{
	Board::Position position;
	auto&[row, col] = position;
	row = 0;
	col = 0;

	if (boardToCheck[position].GetTypePiece() == Piece::TypePiece::None)
		return StateGame::State::Playing;
	Piece::TypePiece pieceType = boardToCheck[position].GetTypePiece();

	for (int index = 1; index < boardToCheck.GetNumberOfRows(); index++)
	{
		row = index;
		col = index;
		if (boardToCheck[position].GetTypePiece() != pieceType)
			return StateGame::State::Playing;
	}

	if (pieceType == Piece::TypePiece::X)
		return StateGame::State::WonX;

	if (pieceType == Piece::TypePiece::O)
		return StateGame::State::WonO;
}

StateGame::State StateGame::CheckDiagonalS(const Board & boardToCheck)
{
	Board::Position position;
	auto&[row, col] = position;
	row = 0;
	col = boardToCheck.GetNumberOfColumns() - 1;

	if (boardToCheck[position].GetTypePiece() == Piece::TypePiece::None)
		return StateGame::State::Playing;
	Piece::TypePiece pieceType = boardToCheck[position].GetTypePiece();

	for (int index = 1; index < boardToCheck.GetNumberOfRows(); index++)
	{
		row = index;
		col = boardToCheck.GetNumberOfColumns() - 1 - index;
		if (boardToCheck[position].GetTypePiece() != pieceType)
			return StateGame::State::Playing;
	}

	if (pieceType == Piece::TypePiece::X)
		return StateGame::State::WonX;

	if (pieceType == Piece::TypePiece::O)
		return StateGame::State::WonO;
}


