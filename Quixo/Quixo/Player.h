#pragma once
#include "../Board/Board.h"
#include "../Piece/Piece.h"

#include <iostream>

class Player
{
public:
	Player();
	Player(const uint8_t& type, const std::string& name);

	const uint8_t GetType() const;
	const std::string& GetName() const;

	std::tuple<Piece, Board::Position> PickPiece(std::istream& in, Board& board) const;
	void PlacePiece(std::istream& in, Piece&& piece, Board& board, const Board::Position & position) const;

private:
	uint8_t m_playerType;
	std::string m_name;
};

