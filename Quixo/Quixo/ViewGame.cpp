#include "ViewGame.h"
#include "ViewBoard.h"

#include<string>

void ViewGame::ShowRules()
{
	std::cout << "GAME RULES" << std::endl;
	std::cout << std::endl;
	std::cout << "On a turn, the active player takes a cube that is blank or bearing his symbol from the outer ring of the grid," << std::endl;
	std::cout << "rotates it so that it shows his symbol(if needed), then adds it to the grid by pushing it into one of the rows" << std::endl;
	std::cout << "from which it was removed. Thus, a few pieces of the grid change places each turn, and the cubes slowly go from" << std::endl;
	std::cout << "blank to crosses and circles. Play continues until someone forms an orthogonal or diagonal line of five cubes" << std::endl;
	std::cout << "bearing his symbol, with this person winning the game." << std::endl;
	std::cout << std::endl;
}

void ViewGame::ShowPlayingBoard(const Board & board)
{
	std::cout << "The board looks like this:\n";
	ViewBoard::ShowBoard(board);
}

void ViewGame::ShowPickingPlayer(const std::reference_wrapper<Player>& player)
{
	std::cout << player.get().GetName() << " is picking a piece from the board." << std::endl;
}

void ViewGame::ShowPlacingPlayer(const std::reference_wrapper<Player>& player)
{
	std::cout << player.get().GetName() << " is placing a piece on the board." << std::endl;
}

void ViewGame::ShowWinner(const std::reference_wrapper<Player>& player)
{
	std::cout << "Player " << player.get().GetName() << " won." << std::endl;
}
