#include "Player.h"

Player::Player()
{
}

Player::Player(const uint8_t & type, const std::string & name) :
	m_playerType(type), m_name(name)
{
}

const uint8_t Player::GetType() const
{
	return m_playerType;
}

const std::string & Player::GetName() const
{
	return m_name;
}

std::tuple<Piece, Board::Position> Player::PickPiece(std::istream & in, Board & board) const
{
	Board::Position pickPiece;
	auto&[line, column] = pickPiece;
	if (in >> line)
		if (in >> column)
		{
			if (line != 0 && line != board.GetNumberOfRows() - 1 && column != 0 && column != board.GetNumberOfColumns() - 1)
				throw "Can not pick piece.";
			Piece piece = board[pickPiece];
			if (piece.GetTypePiece() != Piece::TypePiece::None)
			{
				if (m_playerType == 'X' && piece.GetTypePiece() == Piece::TypePiece::O)
					throw "This is not your piece.";
				if (m_playerType == 'O' && piece.GetTypePiece() == Piece::TypePiece::X)
					throw "This is not your piece.";
			}
			if (m_playerType == 'X')
				piece.SetTypePiece(Piece::TypePiece::X);
			else
				piece.SetTypePiece(Piece::TypePiece::O);
			return std::make_tuple(piece, pickPiece);
		}

	in.clear();
	in.ignore();
	throw "Please enter only two numbers from 0 to 4.";
}

void Player::PlacePiece(std::istream & in, Piece && piece, Board & board, const Board::Position & position) const
{
	Board::Position placePiece;
	auto&[line, column] = placePiece;
	const auto&[initialLine, initialColumn] = position;
	if (in >> line)
		if (in >> column)
		{
			if (position == placePiece)
				throw "You can't rotate the piece.";
			if (initialColumn == column)
			{
				if (line == board.GetNumberOfRows() - 1)
				{
					board.PushUp(position, piece);
					return;
				}
				else
					if (line == 0)
					{
						board.PushDown(position, piece);
						return;
					}
					else
						throw "You can only place it at the end of the column or row.";
			}
			else
				if (initialLine == line)
				{
					if (column == board.GetNumberOfColumns() - 1)
					{
						board.PushLeft(position, piece);
						return;
					}
					else
						if (column == 0)
						{
							board.PushRight(position, piece);
							return;
						}
						else
							throw "You can only place it at the end of the column or row.";
				}
				else
					throw "You can't place it here.";
		}

	in.clear();
	in.ignore();
	throw "Please enter only two numbers from 0 to 4.";
}

