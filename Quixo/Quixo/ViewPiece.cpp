#include "ViewPiece.h"

void ViewPiece::ShowPiece(const Piece & piece)
{
	if (piece.GetTypePiece() == Piece::TypePiece::None)
		std::cout << "-";
	else
		if (piece.GetTypePiece() == Piece::TypePiece::X)
			std::cout << "X";
		else
			std::cout << "O";
}
