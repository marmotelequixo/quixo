#include "ViewBoard.h"
#include "ViewPiece.h"

void ViewBoard::ShowBoard(const Board & board)
{
	Board::Position currentPosition;
	auto&[line, column] = currentPosition;
	for (line = 0; line < board.GetNumberOfRows(); line++)
	{
		for (column = 0; column < board.GetNumberOfColumns(); column++)
		{
			ViewPiece::ShowPiece(board[currentPosition]);
			std::cout << ' ';
		}
		std::cout << std::endl;
	}
}
