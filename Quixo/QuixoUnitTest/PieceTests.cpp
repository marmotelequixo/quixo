#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Piece/Piece.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuixoUnitTest
{
	TEST_CLASS(PieceTests)
	{
	public:

		TEST_METHOD(Constructor)
		{
			Piece piece(Piece::TypePiece::X);
			Assert::IsTrue(piece.GetTypePiece() == Piece::TypePiece::X);
		}

		TEST_METHOD(EqualOperator)
		{
			Piece piece(Piece::TypePiece::X);
			Piece piece2 = piece;
			Assert::IsTrue(piece2.GetTypePiece() == Piece::TypePiece::X);
		}

		TEST_METHOD(EqualMoveOperator)
		{
			Piece piece(Piece::TypePiece::X);
			Piece piece2 = std::move(piece);
			Assert::IsTrue(piece.GetTypePiece() == Piece::TypePiece::None);
		}

		TEST_METHOD(CopyConstructor)
		{
			Piece piece(Piece::TypePiece::X);
			Piece piece2(piece);
			Assert::IsTrue(piece.GetTypePiece() == piece2.GetTypePiece());
		}

		TEST_METHOD(MoveCopyConstructor)
		{
			Piece piece(Piece::TypePiece::X);
			Piece piece2(std::move(piece));
			Assert::IsTrue(piece.GetTypePiece() == Piece::TypePiece::None);
		}
	};
}