#include "stdafx.h"
#include "CppUnitTest.h"
#include "StateGame.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuixoUnitTest
{
	TEST_CLASS(StateGameTests)
	{
	public:

		TEST_METHOD(CheckLineTest)
		{
			Board board;
			Board::Position position;
			auto&[i, j] = position;
			i = 0;
			for (j = 0; j < board.GetNumberOfColumns(); j++)
				board[position].SetTypePiece(Piece::TypePiece::X);
			if (StateGame::CheckLine(board, i) != StateGame::State::WonX)
				Assert::Fail();
		}

		TEST_METHOD(CheckColumnTest)
		{
			Board board;
			Board::Position position;
			auto&[i, j] = position;
			j = 0;
			for (i = 0; i< board.GetNumberOfRows(); i++)
				board[position].SetTypePiece(Piece::TypePiece::X);
			if (StateGame::CheckColumn(board, j) != StateGame::State::WonX)
				Assert::Fail();
		}

		TEST_METHOD(CheckDiagonalPTest)
		{
			Board board;
			Board::Position position;
			auto&[i, j] = position;
			for (i = 0; i < board.GetNumberOfRows(); i++) {
				j = i;
				board[position].SetTypePiece(Piece::TypePiece::X);
			}
			if (StateGame::CheckDiagonalP(board) != StateGame::State::WonX)
				Assert::Fail();
		}

		TEST_METHOD(CheckDiagonalSTest)
		{
			Board board;
			Board::Position position;
			auto&[i, j] = position;
			for (i = 0; i < board.GetNumberOfRows(); i++) {
				j = board.GetNumberOfRows()-1-i;
				board[position].SetTypePiece(Piece::TypePiece::X);
			}
			if (StateGame::CheckDiagonalS(board) != StateGame::State::WonX)
				Assert::Fail();
		}

		TEST_METHOD(CheckTest)
		{
			Board board;
			Board::Position position;
			auto&[i, j] = position;
			j = 0;
			for (i = 0; i < board.GetNumberOfRows(); i++) {
				board[position].SetTypePiece(Piece::TypePiece::X);
			}
			j = 1;
			for (i = 0; i < board.GetNumberOfRows(); i++) {
				board[position].SetTypePiece(Piece::TypePiece::O);
			}
			if(StateGame::Check(board) != StateGame::State::Draw)
				Assert::Fail();
		}
	};
}