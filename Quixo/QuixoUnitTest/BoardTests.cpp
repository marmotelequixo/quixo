#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Board/Board.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuixoUnitTest
{		
	TEST_CLASS(BoardTests)
	{
	public:
		
		TEST_METHOD(DefaultConstructor)
		{
			Board board;
			Board::Position position;
			auto&[i, j] = position;
			for (int i = 0; i < board.GetNumberOfRows(); ++i)
				for (int j = 0; j < board.GetNumberOfColumns(); ++j)
					if (board[position].GetTypePiece()!= Piece::TypePiece:: None)
						Assert::Fail();
		}

		TEST_METHOD(PushDown)
		{
			Piece piece;
			piece.SetTypePiece(Piece::TypePiece::X);
			Board::Position initial = {4,0};
			Board board;
			board.PushDown(initial, piece);
			if (board[{0, 0}].GetTypePiece() != piece.GetTypePiece())
				Assert::Fail();
		}

		TEST_METHOD(PushUp)
		{
			Piece piece;
			piece.SetTypePiece(Piece::TypePiece::X);
			Board::Position initial = { 0,0 };
			Board board;
			board.PushUp(initial, piece);
			if (board[{4, 0}].GetTypePiece() != piece.GetTypePiece())
				Assert::Fail();
		}

		TEST_METHOD(PushLeft)
		{
			Piece piece;
			piece.SetTypePiece(Piece::TypePiece::X);
			Board::Position initial = { 4,0 };
			Board board;
			board.PushLeft(initial, piece);
			if (board[{4, 4}].GetTypePiece() != piece.GetTypePiece())
				Assert::Fail();
		}

		TEST_METHOD(PushRight)
		{
			Piece piece;
			piece.SetTypePiece(Piece::TypePiece::X);
			Board::Position initial = { 4,4 };
			Board board;
			board.PushRight(initial, piece);
			if (board[{4, 0}].GetTypePiece() != piece.GetTypePiece())
				Assert::Fail();
		}

		TEST_METHOD(SquareBracketsOperatorGetterAndSetter)
		{
			Board board;
			Piece piece1;
			piece1.SetTypePiece(Piece::TypePiece::X);
			Piece piece2;
			piece2.SetTypePiece(Piece::TypePiece::O);
			board[{0, 0}] = std::move(piece1);
			Assert::IsTrue(board[{0, 0}].GetTypePiece() == Piece::TypePiece::X);
			int dif = sizeof(&piece2) - sizeof(&board[{0, 0}]);
			Assert::AreEqual(0, dif);
		}

		TEST_METHOD(SquareBracketsConstOperatorGetAtNegativePosition)
		{
			const Board board;
			Assert::ExpectException<std::out_of_range>(
				[&board]() {
				board[{-1, 0}];
			});
			Assert::ExpectException<std::out_of_range>(
				[&board]() {
				board[{0, -1}];
			});
		}
	};
}