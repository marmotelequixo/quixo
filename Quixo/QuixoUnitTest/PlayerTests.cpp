#include "stdafx.h"
#include "CppUnitTest.h"
#include "Player.h"
#include <fstream>
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuixoUnitTest
{
	TEST_CLASS(PlayerTests)
	{
	public:

		TEST_METHOD(Constructor)
		{
			Player playerX('X', "PlayerX");
			Assert::IsTrue(playerX.GetType() == 'X');
			Assert::IsTrue(playerX.GetName() == "PlayerX");
		}

		TEST_METHOD(PickPiece)
		{
			Board board;
			Piece pickedPiece;
			Board::Position position;
			Board::Position positionFromFile = {0,4};
			std::ifstream fin("C:\\Users\\Raluca\\Desktop\\Facultate\\EMC\\quixo\\Quixo\\QuixoUnitTest\\pickpiecetest.txt");
			//std::ifstream fin("pickpiecetest.txt");
			std::streambuf* fin_buf = fin.rdbuf();
			std::streambuf* cin_buf = std::cin.rdbuf(fin_buf);
			Player playerX('X', "PlayerX");
			std::tie(pickedPiece, position) = std::move(playerX.PickPiece(std::cin, board));
			Assert::IsTrue(position == positionFromFile);
			if (pickedPiece.GetTypePiece() != Piece::TypePiece::None)
			{
				if (pickedPiece.GetTypePiece() == Piece::TypePiece::O)
					Assert::Fail();
			}
		}

		TEST_METHOD(PlacePiece)
		{
			Board board;
			Board::Position position = { 0,0 };
			board[position] = Piece::TypePiece::X;
			Piece pickedPiece = board[position];
			Board::Position positionFromFile = { 0,4 };
			std::ifstream fin("C:\\Users\\Raluca\\Desktop\\Facultate\\EMC\\quixo\\Quixo\\QuixoUnitTest\\placepiecetest.txt");
			//std::ifstream fin(".\\Quixo\\QuixoUnitTest\\placepiecetest.txt");
			//std::ifstream fin(".\\placepiecetest.txt");
			std::streambuf* fin_buf = fin.rdbuf();
			std::streambuf* cin_buf = std::cin.rdbuf(fin_buf);
			Player playerX('X', "PlayerX");
			playerX.PlacePiece(std::cin, std::move(pickedPiece), board, position);
			Assert::IsTrue(board[position].GetTypePiece() == Piece::TypePiece::None);
			Assert::IsTrue(board[positionFromFile].GetTypePiece() == Piece::TypePiece::X);
		}
	};
}