#include "Piece.h"

Piece::Piece()
{
	m_typePiece = Piece::TypePiece::None;
}

Piece::Piece(TypePiece typePiece) :
	m_typePiece(typePiece)
{
}

Piece::~Piece()
{
	m_typePiece = Piece::TypePiece::None;
}

Piece& Piece::operator= (const Piece& other)
{
	m_typePiece = other.m_typePiece;
	return *this;
}

Piece& Piece::operator= (Piece&& other)
{
	m_typePiece = other.m_typePiece;
	new(&other) Piece;
	return *this;
}

Piece::Piece(const Piece& other)
{
	*this = other;
}

Piece::Piece(Piece&& other)
{
	*this = std::move(other);
}

Piece::TypePiece Piece::GetTypePiece() const
{
	return m_typePiece;
}

void Piece::SetTypePiece(const TypePiece & type)
{
	m_typePiece = type;
}
